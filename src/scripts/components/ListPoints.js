export class ListPoints {
    constructor() {
        this.list = [];

        this.selectors();
        this.events();
        this.showImage();
    }

    selectors() {
        this.form = document.querySelector('.add__form');
        this.cards = document.querySelector('.list__items');
        this.imageOutput = document.querySelector('.add__form-file-output');
        this.imageInput = document.querySelector('.add__form-file-input');
        this.titleInput = document.querySelector('.add__form-inputs-title');
        this.descInput = document.querySelector('.add__form-inputs-desc');
        this.imageCard = document.querySelector('.list__items-card-image');
    }

    events() {
        this.form.addEventListener('submit', this.handleAddItems.bind(this));
    }

    handleAddItems(event) {
        event.preventDefault();

        if (this.imageInput.value !== '' && this.titleInput.value !== '' && this.descInput.value !== '') {
            const item = {
                imageSrc: this.imageOutput.url,
                title: this.titleInput.value,
                description: this.descInput.value
            }

            this.list.push(item)

            console.log(this.list);

            console.log(this.showImage());

            this.renderList();
            this.formatList();
        }
    };

    showImage() {
        this.imageOutput.onclick = () => this.imageInput.click();

        this.imageInput.onchange = e => {
            const fileToUpload = e.target.files.item(0);
            const reader = new FileReader();
            reader.onload = (e) => {
                this.imageOutput.url = e.target.result;

                this.imageOutput.innerHTML = `
                    <img class="add__form-file-img" src="${this.imageOutput.url}" alt="${this.imageOutput.url.name}"/>
                `
            }
            reader.readAsDataURL(fileToUpload);
        };
    }

    sendImage() {
        this.form.onsubmit = () => {
            const fileToUpload = this.imageInput.files.item(0);
            const reader = new FileReader();
            reader.onload = (e) => {
                this.imageCard.src = e.target.result;
            }
            reader.readAsDataURL(fileToUpload);

        };
    }

    renderList() {
        let itemsStructure = '';

        this.list.forEach(item => {
            itemsStructure = `
                <div class="list__items-card">
                    <div>
                        <img
                            class="list__items-card-image"
                            src="${item.imageSrc}"
                            alt="${item.title}"
                        />
                    </div>
                    <div class="list__items-card-text">
                        <h3 class="list__items-card-title">
                            ${item.title}
                        </h3>
                        <p class="list__items-card-desc">
                            ${item.description}
                        </p>
                    </div>
                </div>
            `;
        });

        this.cards.innerHTML += itemsStructure;
    };

    formatList() {
        this.imageOutput.value = '';
        this.titleInput.value = '';
        this.descInput.value = '';
    };
}
